// gcc -o practica1_2 -Wall -Werror practica1_2.c 
#include <stdio.h>
#include <stdlib.h>

int string_len(char * str){
    int i = 0;
    while(str[i] != '\0'){i++;}
    return i;
}

void string_reverse(char* str){
    int n = string_len(str), m = n/2;
    char c;
    for (int i = 0; i  < m; i++){c = str[i]; str[i] = str[n-1-i]; str[n-1-i] = c;}
}

int minimo(int n, int m){
    if (n < m){
        return n;
    } else {
        return m;
    }
}

//se asume que ambos strings o ertan en mayusculas o estan en minusculas
int string_compare(char* str1, char* str2){
    int salida = 0;
    int n = minimo(string_len(str1),string_len(str2));

    for(int i = 0, cond_salida = 1; (i < n) && (cond_salida) && (str1[i] != '\0') && (str2[i] != '\0'); i++){
        if (str1[i] < str2[i]){
            salida = -1;
            cond_salida = 0; 
        }else if (str1[i] > str2[i]){
            salida = 1;
            cond_salida = 0;
        }
    }
    return salida;
}

int string_concat(char* str1, char* str2, int max){
	return 1;
	}
	
int string_subcadena(char* str1, char* str2){
	int larg1 =  string_len(str1);
	int larg2 =  string_len(str2);
	int larg = larg1 - larg2 + 1;
	int salida = -1, roptura = 1;
	//printf("%d, %d, %d\n", larg1,larg2,larg);
    for (int i = 0, j = 0, k = 0; i < larg1 && roptura; i++, j++){
        //printf("%d, %d, %d\n", i, j, k);
        if (str1[i] == str2[j]){
            k++;
        }else{
            k = 0;
            j = -1;
            if (larg < i){roptura = 0;}
        }
        if (k == larg2){salida = i - larg2 + 1; roptura = 0;/* printf("Salida: %d, %d, %d\n",salida,i, k);*/}

    }
	return salida;
	}

//Se asume que capacidad contine la cantidad de string de arreglosStrings.
//
void string_unir(char* arregloStrings[], int capacidad, char* sep, char* res){
    int cant_total[capacidad], sum_cant_total = 0,cant_sep = string_len(res);
    for(int i = 0; i < capacidad; i++){
        cant_total[i] = string_len(arregloStrings[i]);
        sum_cant_total += cant_total[i];
    }

    sum_cant_total += (cant_sep * (capacidad - 1));

    res = malloc(sizeof(char) * (sum_cant_total + 1));

    for(int i = 0, k = 0; i < capacidad; i++){
        for(int j = 0; j < cant_total[i];j++, k++){
            res[k] = * arregloStrings[j];
        }

        for (int j = 0; j < cant_sep; j++, k++){
            res[k] = sep[j];
        }
    }
}

void rellenar(char * a[], int cant_elem){
    char buf[100];
    for (int i = 0, cant; i < cant_elem; i++){
        scanf("%s", buf);
        cant = string_len(buf);
        printf("%d", cant);
        a[i] = malloc(sizeof(char) * (cant + 1));
        a[i][cant] = '\0';
        printf("%d, %d, ", i, cant);
        sscanf(buf, "%s", a[i]);
        printf("%s\n", a[i]);
    }

    printf("%s",buf);
}

int main(){
    char str[] = {"mariano"};
    char str2[] = {"mariane"};
    char str1[] = {"mariana"};
    printf("%d\n",string_len(str));
    string_reverse(str);
    printf("%s\n", str);
    printf("%d\n",string_compare(str1,str2));
    char a[] = {"trasendental"}, b[] = {"etal"};
    printf("%d\n",string_subcadena(a,b));
    
    int elem = 5;
    char* array[elem];
    char* r = NULL;
    char space[] = {"  "};
    
    rellenar(array, elem);
    //for(int i; i < elem; i++){printf("%s\n",array[i]);}
    string_unir(array, elem, r, space);
    printf("%s", r);
    return 1;
}
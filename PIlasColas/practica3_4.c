//gcc -o practica3_4 -Werror -Wall -g practica3_4.c
#include <stdio.h>
#include <stdlib.h>

typedef void (*FuncionVisitante) (int dato);

static void imprimir_entero(int dato) {
  printf("%d ", dato);
}

typedef struct _SNodo {
  int dato;
  struct _SNodo *sig;
} SNodo;

typedef SNodo *SList;

SList slist_crear() {
  return NULL;
}

void slist_destruir(SList lista) {
  SNodo *nodoAEliminar;
  while (lista != NULL) {
    nodoAEliminar = lista;
    lista = lista->sig;
    free(nodoAEliminar);
  }
}

int slist_vacia(SList lista) {
  return lista == NULL;
}

SList slist_agregar_final(SList lista, int dato) {
  SNodo *nuevoNodo = malloc(sizeof(SNodo));
  nuevoNodo->dato = dato;
  nuevoNodo->sig = NULL;

  if (lista == NULL)
    return nuevoNodo;

  SList nodo = lista;
  for (;nodo->sig != NULL;nodo = nodo->sig);
  /* ahora 'nodo' apunta al ultimo elemento en la lista */

  nodo->sig = nuevoNodo;
  return lista;
}

SList slist_agregar_inicio(SList lista, int dato) {
  SNodo *nuevoNodo = malloc(sizeof(SNodo));
  nuevoNodo->dato = dato;
  nuevoNodo->sig = lista;
  return nuevoNodo;
}

void slist_recorrer(SList lista, FuncionVisitante visit) {
  for (SNodo *nodo = lista; nodo != NULL; nodo = nodo->sig)
    visit(nodo->dato);
}

void slist_eliminar_nodo(SList lista){
    if (lista != NULL){
        if (lista->sig != NULL){
            SList liberar = lista->sig;
            lista->sig = liberar->sig;
            lista->dato = liberar->dato;
            free(liberar);
        }else{
            free(lista);
            lista = NULL;
        }
    }
}

// Ejer 3.3

//typedef SNodo Pilar;
typedef SList Pila;

Pila pila_crear(){
    return slist_crear();
}

void pila_destruir(Pila pil){
    slist_destruir(pil);
}

int pila_vacia(Pila pil){
    return pil == NULL;
}

int pila_ultimo(Pila pil){
    return pil->dato;
}

Pila pila_apilar(Pila pil, int dato){
    return slist_agregar_inicio(pil,dato);
}

void pila_desapilar(Pila pil){
    slist_eliminar_nodo(pil);
}

void pila_imprimir(Pila pil){
    //for (int i = pil->ultimo; i >= 0; i--)printf("%d ",pil->datos[i]);
    slist_recorrer(pil, imprimir_entero);
    printf("\n");
}

//Ejer 4


int main(){
    //printf("hola\n");

    Pila pil = pila_crear();
    SList lista = slist_crear();

    for(int i = 0; i < 10; i++){
        lista = slist_agregar_inicio(lista, i);
        printf("%d\n",i);
    }

    slist_recorrer(lista, imprimir_entero);
    
    while (!slist_vacia(lista)){
        printf("hola");
        pil = pila_apilar(pil,lista->dato);
        lista = slist_eliminar_nodo(lista);
    }
    /*
    pila_imprimir(pil);

    while (!pila_vacia(pil)){
        lista = slist_agregar_final(lista, pila_ultimo(pil));
        pila_desapilar(pil);
    }
    
    slist_recorrer(lista, imprimir_entero);
    */
    slist_destruir(lista);
    pila_destruir(pil);
    
    return 0;
}
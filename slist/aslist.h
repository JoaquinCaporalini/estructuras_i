#ifndef __ASLIST_H__
#define __ASLIST_H__

#include <stddef.h>

typedef struct _ANodo{
    int dato, siguiente;
} ANodo;

typedef ANodo *ASArray;
typedef int ASList;

//dado un tamanio, crea el espacio para los nodos 
ASArray asarray_crear(int tamnio);

//dado un arreglo y su tamanio los indexa
int asarray_index(ASArray nodos, int tamanio);

//Dado una arreglo de nodos y su numero de avail te devuelve un nodo disponible
void aslist_nodo(ASArray nodos, int * avail);

//Dado una arreglo de nodos, su avail y el nodo a liverar
void aslist_free(ASArray nodos, int * avail, int posicion);

//Dado un arreglo de nodos lo libera
void asarray_free(ASArray nodos);


#endif /* __ASLIST_H__ */
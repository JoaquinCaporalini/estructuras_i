#include "btree.h"
#include "cola.h"
#include <stdio.h>
#include <stdlib.h>

BTree btree_crear() {
  return NULL;
}

void btree_destruir(BTree nodo) {
  if (nodo != NULL) {
    btree_destruir(nodo->left);
    btree_destruir(nodo->right);
    free(nodo);
  }
}

int btree_empty(BTree nodo) {
  return nodo == NULL;
}

BTree btree_unir(int dato, BTree left, BTree right) {
  BTree nuevoNodo = malloc(sizeof(BTNodo));
  nuevoNodo->dato = dato;
  nuevoNodo->left = left;
  nuevoNodo->right = right;
  return nuevoNodo;
}

void in_order(BTree arbol, FuncionVisitante visit){
		if (!btree_empty(arbol)){
			in_order(arbol->left, visit);	//Izq
			visit(arbol->dato);             //Raiz
			in_order(arbol->right, visit);	//Der
		}
}

void pre_order(BTree arbol, FuncionVisitante visit){
		if (!btree_empty(arbol)){
			visit(arbol->dato);             //Raiz
			pre_order(arbol->left, visit);	//Izq
			pre_order(arbol->right, visit);	//Der
		}
}

void post_order(BTree arbol, FuncionVisitante visit){
		if (!btree_empty(arbol)){
			post_order(arbol->left, visit);	//Izq
			post_order(arbol->right, visit);//Der
			visit(arbol->dato);             //Raiz
		}
}

void btree_recorrer(BTree arbol, BTreeOrdenDeRecorrido orden,
                    FuncionVisitante visit){
	if (!btree_empty(arbol)){
		switch(orden) {
      case BTREE_RECORRIDO_IN :
         in_order(arbol, visit);
         break;
      case BTREE_RECORRIDO_PRE :
         pre_order(arbol, visit);
         break;
      case BTREE_RECORRIDO_POST :
         post_order(arbol, visit);
         break;
      default :
         printf("Invalid algori\n" );
		}
	}
}

void in_order_extra(BTree arbol, 
										FuncionVisitanteExtra visit, void *extra){
		if (!btree_empty(arbol)){
			in_order_extra(arbol->left, visit, extra);	//Izq
			visit(arbol->dato, extra);       			//Raiz
			in_order_extra(arbol->right, visit, extra);	//Der
		}
}

void pre_order_extra(BTree arbol, 
										FuncionVisitanteExtra visit, void *extra){
		if (!btree_empty(arbol)){
			visit(arbol->dato, extra);       			//Raiz
			pre_order_extra(arbol->left, visit, extra);	//Izq
			pre_order_extra(arbol->right, visit, extra);//Der
		}
}

void post_order_extra(BTree arbol, 
										FuncionVisitanteExtra visit, void *extra){
		if (!btree_empty(arbol)){
			post_order_extra(arbol->left, visit, extra);	//Izq
			post_order_extra(arbol->right, visit, extra);	//Der
			visit(arbol->dato, extra);       				//Raiz
		}
}

void btree_recorrer_extra(BTree arbol, BTreeOrdenDeRecorrido orden, 
													FuncionVisitanteExtra visit, void *extra){
	if (!btree_empty(arbol)){
		switch(orden) {
      case BTREE_RECORRIDO_IN :
         in_order_extra(arbol, visit, extra);
         break;
      case BTREE_RECORRIDO_PRE :
         pre_order_extra(arbol, visit, extra);
         break;
      case BTREE_RECORRIDO_POST :
         post_order_extra(arbol, visit, extra);
         break;
      default :
         printf("Invalid algori\n" );
		}
	}
}

void btree_recorrer_bfs (BTree arbol, 
						FuncionVisitanteExtra visit, void *extra){
	if (!btree_empty(arbol)) {
		Cola cola = cola_crear();
		cola_encolar(cola, arbol);
		BTree nodo;
		while (!cola_vacia(cola)) {
			nodo = cola_desencolar(cola);
			if (nodo->left != NULL)
				cola_encolar(cola, nodo->left);
			if (nodo->right != NULL)
				cola_encolar(cola, nodo->right);
			visit(nodo->dato, extra);
		}
		cola_destruir(cola);
	}
}





















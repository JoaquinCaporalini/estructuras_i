//gcc -o practica3_2 -Werror -Wall -g practica3_2.c
#include <stdlib.h>
#include <stdio.h>
#define MAX_PILA 10

typedef struct _Pila {
    int* datos;
    int tamanio;
    int ultimo;
} Pilar;

typedef Pilar *Pila;

Pila pila_crear(){
    Pila pil = malloc(sizeof(Pilar));
    pil->datos = malloc(sizeof(int) * MAX_PILA);
    pil->tamanio = 10;
    pil->ultimo = -1;
    return pil;
}

void pila_destruir(Pila pil){
    free(pil->datos);
    free(pil);
}

int pila_vacia(Pila pil){
    return pil->ultimo == -1;
}

int pila_ultimo(Pila pil){
    return pil->ultimo;
}

void pila_apilar(Pila pil, int dato){
    int ultimo = pil->ultimo;
    int tamanio = pil->tamanio;
    if(ultimo + 1 == tamanio){
        pil->datos = realloc(pil->datos, sizeof(int) * (tamanio + 10));
        pil->tamanio = tamanio  + 10;
    }

    ultimo++;
    pil->datos[ultimo] = dato;
    pil->ultimo = ultimo;
    
}

void pila_desapilar(Pila pil){
    int ultimo = pil->ultimo -1;
    pil->ultimo = ultimo;
}

void pila_imprimir(Pila pil){
    for (int i = pil->ultimo; i >= 0; i--)printf("%d ",pil->datos[i]);
    printf("\n");
}

int main(){
    //printf("hola\n");

    Pila pil = pila_crear();
    
    for (int i = 0; i < MAX_PILA;i++){
        pila_apilar(pil,i);
    }
    
    pila_imprimir(pil);
    
    
    for (int i = 0; i < 2; i++){
        printf("%d ",pila_ultimo(pil));
        printf("%d\n", pila_ultimo(pil));
        pila_desapilar(pil);
    }
    
    pila_imprimir(pil);

    pila_destruir(pil);
    
    return 0;
}
// gcc -o practica1_3 -Wall -Werror practica1_3.c 
#include <stdio.h>
#include <stdlib.h>

typedef struct {
    float* matriz;
    int filas;
    int columnas;
} Matriz;

int btou(Matriz * mtrx, int fila, int columna){
    
    return (*mtrx).columnas * fila + columna;
}

Matriz * matriz_crear(int filas, int columnas){
    Matriz * mtrx = malloc(sizeof(Matriz));
    (*mtrx).matriz = malloc(sizeof(float) * (columnas * filas));
    (*mtrx).columnas = columnas;
    (*mtrx).filas = filas;
    return mtrx;
}

void matriz_destruir(Matriz * mtrx){
    free((* mtrx).matriz);
    free(mtrx);
    mtrx = NULL;
}

void matriz_imprimir(Matriz * mtrx){
    float * mtrxF = (*mtrx).matriz;
    for (int i = 0; i < (*mtrx).filas;i++){
        for (int j = 0, pos; j < (*mtrx).columnas; j++){
            pos = (*mtrx).columnas * i + j;
            printf("%f ", mtrxF[pos]);
        }
        printf("\n");
    }
}

float leer_matriz(Matriz * mtrx, int fila, int columan){
    int pos =  (*mtrx).columnas * fila + columan;
    return (*mtrx).matriz[pos];
}

void escribir_matriz(Matriz * mtrx, int fila, int columan, float dato){
    int pos =  (*mtrx).columnas * fila + columan;
    (*mtrx).matriz[pos] = dato;
}

int capacidad_filas_matriz(Matriz * mtrx){
    return (*mtrx).filas;
}

int capacidad_columnas_matriz(Matriz * mtrx){
    return (*mtrx).columnas;
}

void intercambiar_columnas_matriz(Matriz * mtrx, int origen, int destino){
    float swap;
    for (int i = 0; i < capacidad_filas_matriz(mtrx);i++){
        swap = (*mtrx).matriz[btou(mtrx, i, origen)];
        (*mtrx).matriz[btou(mtrx, i, origen)] = (*mtrx).matriz [btou(mtrx, i, destino)];
        (*mtrx).matriz[btou(mtrx, i, destino)] = swap;
    }
}

void intercambiar_filas_matriz(Matriz * mtrx, int origen, int destino){
    float swap;
    for (int i = 0; i < capacidad_columnas_matriz(mtrx);i++){
        swap = (*mtrx).matriz[btou(mtrx, origen, i)];
        (*mtrx).matriz[btou(mtrx, origen, i)] = (*mtrx).matriz[btou(mtrx, destino, i)];
        (*mtrx).matriz[btou(mtrx, destino, i)] = swap;
    }
}

void incertar_fila_matriz(Matriz * mtrx, int pos){
    if ((*mtrx).filas >= pos && pos >= 0){
        (*mtrx).matriz = realloc((*mtrx).matriz, sizeof(float) * (((*mtrx).filas + 1) * (*mtrx).columnas));
        for(int i = (*mtrx).filas - 1; i >= pos; i--){
            for (int j = 0; j < (*mtrx).columnas; j++){
                escribir_matriz(mtrx, i+1, j, leer_matriz(mtrx, i, j));
            }
        }
        (*mtrx).filas++;
    }
}

Matriz * sumar_matrices(Matriz * mtrx1, Matriz * mtrx2){
    Matriz * mtrx = NULL;
    if((*mtrx1).filas == (*mtrx2).filas){
        if((*mtrx1).columnas == (*mtrx2).columnas){
            mtrx = matriz_crear((*mtrx1).filas, (*mtrx1).columnas); 
            for(int i = 0; i < (*mtrx1).filas;i++){
                for (int j = 0; j < (*mtrx1).columnas; j++){
                    (*mtrx).matriz[btou(mtrx,i,j)] = (*mtrx1).matriz[btou(mtrx1,i,j)] + (*mtrx2).matriz[btou(mtrx2, i,j)];
                }
            }
        }
    }
    return mtrx;
}

Matriz * multiplicar_matrices(Matriz * mtrx1, Matriz * mtrx2){
    Matriz * mtrx = NULL;
    if((*mtrx1).columnas == (*mtrx2).filas){
        mtrx = matriz_crear((*mtrx1).filas, (*mtrx2).columnas);
        float mult = 0;
        for(int i = 0; i < (*mtrx1).filas; i++){
            for (int j = 0; j < (*mtrx2).columnas; j++){
                for(int k = 0; k < (*mtrx1).columnas;k++){
                    mult += leer_matriz(mtrx1, i, k) * leer_matriz(mtrx1, k, j);
                }
                escribir_matriz(mtrx,i,j,mult);
                mult = 0;   
            }
        }
    }
    return mtrx;
}

int main(){

    Matriz * mtrx1 = matriz_crear(2,2);
    Matriz * mtrx2 = matriz_crear(2,2); 
    Matriz * mtrx;

    //incertar_fila_matriz(mtrx, 2);

    for (int i = 0; i < capacidad_filas_matriz(mtrx1); i++){
        for (int j = 0; j < capacidad_columnas_matriz(mtrx1);j++){
            escribir_matriz(mtrx1, i, j, i + j);
        }
    }

    for (int i = 0; i < capacidad_filas_matriz(mtrx2); i++){
        for (int j = 0; j < capacidad_columnas_matriz(mtrx2);j++){
            escribir_matriz(mtrx2, i, j, i + j);
        }
    }
    
    mtrx = multiplicar_matrices(mtrx1, mtrx2);

    matriz_imprimir(mtrx);
    printf("\n");
    matriz_destruir(mtrx);

    mtrx = sumar_matrices(mtrx1, mtrx2);

    matriz_imprimir(mtrx);
    printf("\n");
    matriz_destruir(mtrx);
    
    matriz_destruir(mtrx1);
    matriz_destruir(mtrx2);
    
    /*
    intercambiar_columnas_matriz(mtrx, 0, 2);
    matriz_imprimir(mtrx);
    printf("\n");

    intercambiar_filas_matriz(mtrx,0,4);
    matriz_imprimir(mtrx);
    printf("\n");
    

    matriz_destruir(mtrx);
    */
    

    return 1;
}
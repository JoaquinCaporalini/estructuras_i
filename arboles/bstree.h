#ifndef __BSTREE_H__
#define __BSTREE_H__

typedef void (*FuncionVisitanteExtra) (int dato, void *extra);

typedef struct _BSTNodo {
  int dato;
  struct _BSTNodo *left;
  struct _BSTNodo *right;
} BSTNodo;

typedef BSTNodo *BSTree;

/**
 * Devuelve un arbol vacío.
 */
BSTree bstree_crear();

/**
 * Destruccion del árbol.
 */
void bstree_destruir(BSTree nodo);

/**
 * Indica si el árbol es vacío.
 */
int bstree_empty(BSTree nodo);

/**
 * Crea un nuevo arbol, con el dato dado en el nodo raiz, y los subarboles dados
 * a izquierda y derecha.
 */
BSTree bstree_unir(int dato, BSTree left, BSTree right);

BSTree bstree_agregar(BSTree arbol, int dato);

BSTree bstree_eliminar(BSTree arbol, int dato);

int bstree_len(BSTree arbol);

int bstree_altura(BSTree arbol, int i);

void bstree_recorrer_bfs (BSTree arbol, 
						FuncionVisitanteExtra visit, void *extra);


#endif /* __BSTREE_H__ */


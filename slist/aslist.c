//gcc -c slist.c
#include "aslist.h"
#include <stdlib.h>

//dado un tamanio, crea el espacio para los nodos 
ASArray asarray_crear(int tamnio){
    ASArray nodos = malloc(sizeof(ANodo) * tamnio);
    asarray_index(nodos, tamnio);
    return nodos;
}

//dado un arreglo y su tamanio los indexa
int asarray_index(ASArray nodos, int tamanio){
    if(nodos != NULL){
        for(int i = 0; i < tamanio -1 ;i++){
            nodos[i].siguiente = i+1;
        }
        nodos[tamanio-1].siguiente = -1;
        return 0;
    }
    exit(1);
}

//Dado una arreglo de nodos y su numero de avail te devuelve un nodo disponible
int aslist_nodo(ASArray nodos, int * avail){
    int salida = *avail;
    *avail = nodos[salida].siguiente;
    return salida;
}

//Dado una arreglo de nodos, su avail y el nodo a liverar
void aslist_free(ASArray nodos, int * avail, int posicion){
    nodos[posicion].siguiente = *avail;
    *avail = posicion;
}

//Dado un arreglo de nodos lo libera
void asarray_free(ASArray nodos){
    free(nodos);
    nodos = NULL;
}

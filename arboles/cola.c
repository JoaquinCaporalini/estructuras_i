#include "cola.h"
#include <stdio.h>
#include <stdlib.h>

Cola cola_crear(){
	Cola cola = malloc(sizeof(struct _Cola));
	cola->primero = NULL;
	cola->ultimo = NULL;
	return cola;
}

int cola_vacia(Cola cola){
	return cola->primero == NULL;
}

void cola_encolar(Cola cola, void * dato){
	CNodo * nodo = malloc(sizeof(CNodo));
	nodo->dato = dato;
	nodo->sig = NULL;
	
	if (cola_vacia(cola)){
		cola->primero = nodo;
		cola->ultimo = nodo;
	}
	else {
		cola->ultimo->sig = nodo;
		cola->ultimo = nodo;
	}
}

void cola_destruir(Cola cola){
	CNodo * nodo;
	while (cola->primero != NULL) {
		nodo = cola->primero->sig;
		free(cola->primero);
		cola->primero = nodo;
	}
	free(cola);
}

void * cola_desencolar(Cola cola){
	void * salida = cola->primero->dato;
	
	if (cola_vacia(cola)){
		cola_destruir(cola);
		exit(1);
	}
	CNodo * nodo = cola->primero;
	cola->primero = nodo->sig;		
	free(nodo);
	return salida;
}
/*
void cola_recorrer(Cola cola){
	CNodo * nodo = cola->primero;
	while(nodo != NULL){
		printf("%d ", nodo->dato);
		nodo = nodo->sig;
	}
	printf("\n");
}
*/

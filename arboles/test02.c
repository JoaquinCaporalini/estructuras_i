#include <stdio.h>
#include <stdlib.h>
#include "bstree.h"

static void mostrame(int dato, void * resta) {
  printf("%d ", dato - *(int *)resta);
}

int main() {
	BSTree arbol = bstree_crear();
	int a = 0;
	for (int i = 0; i<10; i++) {
		bstree_agregar(arbol, i);
	}
	bstree_recorrer_bfs(arbol, mostrame, &a);
	printf("Hola\n");
	bstree_destruir(arbol);
	return 0;
}


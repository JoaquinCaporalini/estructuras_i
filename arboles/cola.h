#ifndef __COLA_H__
#define __COLA_H__

typedef struct _CNodo {
  void * dato;
  struct _CNodo *sig;
} CNodo;

typedef struct _Cola {
	CNodo * primero;
	CNodo * ultimo;  
} * Cola;



Cola cola_crear();

int cola_vacia(Cola cola);

void cola_encolar(Cola cola, void * dato);

void cola_destruir(Cola cola);

void * cola_desencolar(Cola cola);

//void cola_recorrer(Cola cola);

#endif /* __BTREE_H__ */

//gcc -o practica3_5 -Werror -Wall -g practica3_5.c

#include <stdlib.h>
#include <stdio.h>
#define MAX_COLA 10

typedef struct _Cola {
    int datos[MAX_COLA];
    int primero, ultimo;
} CNodo;

typedef CNodo *Cola;

//crea e inicializa una nueva cola vac´ıa.
Cola cola_crear(){
    Cola col = malloc(sizeof(CNodo));
    col->primero = -1;
    col->ultimo = -1;
    return col;
}

//libera la memoria requerida para la cola
void cola_destruir(Cola col){
    free(col);
}

//determina si la cola est´a vac´ıa.
int cola_es_vacia(Cola col){
    return col->primero == -1;
} 

//toma una cola y devuelve el elemento en la primera posici´on.
int cola_primero(Cola cola){
    int pos_prim = cola->primero;
    if (pos_prim != -1){
        return cola->datos[pos_prim % MAX_COLA];
    }
    else{
        printf("No existe primer elemento de una cola vacia\n");
        cola_destruir(cola);
        exit(0);
    }
}

//toma una cola y un elemento y agrega el elemento al fin de la cola.
void cola_encolar(Cola cola, int dato){
    if (cola->primero == -1){
        cola->primero = 0;
        cola->ultimo = 0;
        cola->datos[0] = dato;
    }
    else{
        if((cola->ultimo +1) % MAX_COLA != cola->primero % MAX_COLA){
            cola->ultimo++;
            cola->datos[cola->ultimo % MAX_COLA] = dato;
        } else{
            printf("Overflow\n");
            cola_destruir(cola);
            exit(0);
        }

    }
    
}

//toma una cola y elimina su primer elemento.
void cola_desencolar(Cola cola){
    if (cola->primero == -1){
            printf("Pericion fuera de rango\n");
            cola_destruir(cola);
            exit(0);
    }
    else{
        if(cola->primero < cola->ultimo){
            cola->primero++;
        } else{
            cola->ultimo = -1;
            cola->primero = -1;
        }
    } 
}

//toma una cola y la imprime en orden.
void cola_imprimir(Cola cola){
    if (cola->primero == -1){
        printf("Cola vacia\n");
        cola_destruir(cola);
        exit(1);
    }
    for (int i = cola->primero; i <= cola->ultimo; i++){printf("%d ", cola->datos[i%MAX_COLA]);}
    printf("\n");
}

int main (){
    Cola cola = cola_crear();

    for(int i = 0; i < MAX_COLA+1; i++){
        cola_encolar(cola, i);
    }

    cola_imprimir(cola);

    for(int i = 0; i < 2; i++){
        printf("%d\n", cola_primero(cola));
        cola_desencolar(cola);
        cola_encolar(cola, 10+i);
    }

    cola_imprimir(cola);

    for(int i = 0; i < MAX_COLA; i++){
        printf("%d %d\n", cola_primero(cola), i);
        cola_desencolar(cola);
        //cola_encolar(cola, 10+i);
    }

    printf("%d \n\n", cola_es_vacia(cola));

    cola_destruir(cola);

    return 0;
}
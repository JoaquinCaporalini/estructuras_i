//gcc -o practica2_2 -Werror -Wall -g practica2_2.c slist.o

#include <stdlib.h>
#include <stdio.h>
#include "slist.h"

int slist_longitud(SList * lista){
    int contador = 0;
    SList pos = (* lista);
    if (lista != NULL){
        for(;pos->sig != NULL;contador++){    
            pos = pos->sig; 
        }
    }
    return contador;
}

// NULL <- NULL
// NULL <- LIST
// LIST <- NULL
// LIST <- LIST 
void slist_concatenar(SList lista1, SList lista2){
    if (lista1 == NULL && lista2 != NULL){
        lista1 = lista2;
    } else if (lista1 != NULL && lista2 != NULL){
        SList pos = lista1;
        for(;pos->sig != NULL; pos =  pos->sig);
        pos->sig = lista2;
    }
}

void slist_insertar_nodo(SList lista, int dato, int posicion){
    if (lista != NULL){
        if (posicion == 0){
            //slist_agregar_inicio(lista, dato);
            SNodo *nuevoNodo = malloc(sizeof(SNodo));
            nuevoNodo->dato = lista->dato;
            nuevoNodo->sig = lista->sig;
            lista->sig = nuevoNodo;
            lista->dato = dato;
        }else{
            for(; (1 < posicion) && (lista->sig != NULL);){
                posicion--; lista = lista->sig;
                }
            if (posicion == 1){
                printf("pos->ENCONTRADA\n");
                SNodo *nuevoNodo = malloc(sizeof(SNodo));
                nuevoNodo->dato = dato;
                nuevoNodo->sig = lista->sig;
                lista->sig = nuevoNodo;
            }else{
                printf("pos->NO ENCONTRADA **** RESTANTE: %d\n",posicion);
                printf("%p\n",lista->sig);

            }
        }
    }
}

void slist_insertar(SList lista, int dato, int posicion){
    if (lista != NULL){
        if (posicion == 0){
            lista->dato = dato;
        }else{
            for(; (0 < posicion) && (lista->sig != NULL);){
                posicion--; lista = lista->sig;
                };
            if (posicion == 0){
                printf("pos->ENCONTRADA\n");
                lista->dato = dato;
            }else{
                printf("pos->NO ENCONTRADA **** RESTANTE: %d\n",posicion);
                printf("%p\n",lista->sig);

            }
        }
        
    }
}

//pop de python
void slist_eliminar_nodo(SList lista, int posicion){
    if (lista != NULL){
        if (lista->sig != NULL){
            if (posicion == 0){
                SList liberar = lista->sig;
                lista->sig = liberar->sig;
                lista->dato = liberar->dato;
                free(liberar);
            }else{
                for(;(1 < posicion) && (lista->sig != NULL);){
                    posicion--;lista = lista->sig;
                }
                if (posicion == 1){
                    if(lista->sig != NULL){
                        SList liberar = lista->sig;
                        lista->sig = liberar->sig;
                        lista->dato = liberar->dato;
                        free(liberar);
                    }
                }else{
                    printf("pos->NO ENCONTRADA **** RESTANTE: %d\n",posicion);
                }
            }
        }else{
            free(lista);
            lista = NULL;
        }
    }
}

int slist_contiene(SList lista, int dato){
    int encontado = 0;
    for(;!encontado && lista != NULL;lista = lista->sig){
        if(lista->dato == dato){
            encontado = 1;
        } 
    }
    return encontado;
}

int slist_indice(SList lista, int dato){
    int encontrado = -1;
    for(int i = 0; lista != NULL && encontrado == -1; i++, lista = lista->sig){
        if (lista->dato == dato){
            encontrado = i;
            }
    }
    
    return encontrado;
}

static void imprimir_entero(int dato) {
  printf("%d ", dato);
}

int main(){
    SList lista1 = slist_crear(), lista2 = slist_crear();

    for(int i = 4; i > -1; i--){
        lista1 = slist_agregar_inicio(lista1, 2 * i);
        lista2 = slist_agregar_inicio(lista2, 2 * i + 1);
    }

    printf("4 -> %d\n",slist_indice(lista1, 4));
    printf("8 -> %d\n",slist_indice(lista1, 8));
    printf("0 -> %d\n",slist_indice(lista1, 0));
    printf("3 -> %d\n",slist_indice(lista1, 3));


    slist_recorrer(lista1,imprimir_entero);
    puts("");
    //slist_recorrer(lista2,imprimir_entero);
    puts("");

    slist_destruir(lista1);
    slist_destruir(lista2);

    puts("");

    return 1;
}
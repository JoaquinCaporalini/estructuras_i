// gcc -o practica1_3 -Wall -Werror practica1_3.c 
#include <stdio.h>
#include <stdlib.h>

typedef struct {
    float** matriz;
    int filas;
    int columnas;
} Matriz;

Matriz * matriz_crear(int filas, int columnas){
    Matriz * mtrx = malloc(sizeof(Matriz));
    (*mtrx).matriz = malloc(sizeof(float *) * filas);
    for(int i = 0; i < filas; i++){
        (*mtrx).matriz[i] = malloc(sizeof(float) * columnas);
    }
    (*mtrx).columnas = columnas;
    (*mtrx).filas = filas;
    return mtrx;
}

void matriz_destruir(Matriz * mtrx){
    for(int i = 0; i < (*mtrx).filas; i++){
        free((*mtrx).matriz[i]);
    }
    free((*mtrx).matriz);
    free(mtrx);
    mtrx = NULL;
}

void matriz_imprimir(Matriz * mtrx){
    for (int i = 0; i < (*mtrx).filas;i++){
        for (int j = 0; j < (*mtrx).columnas; j++){
            printf("%f ", (*mtrx).matriz[i][j]);
        }
        printf("\n");
    }
}

float leer_matriz(Matriz * mtrx, int fila, int columan){
    return (*mtrx).matriz[fila][columan];
}

void escribir_matriz(Matriz * mtrx, int fila, int columan, float dato){
    (*mtrx).matriz[fila][columan] = dato;
}

int capacidad_filas_matriz(Matriz * mtrx){
    return (*mtrx).filas;
}

int capacidad_columnas_matriz(Matriz * mtrx){
    return (*mtrx).columnas;
}

void intercambiar_columnas_matriz(Matriz * mtrx, int origen, int destino){
    float swap;
    for (int i = 0; i < capacidad_filas_matriz(mtrx);i++){
        swap = (*mtrx).matriz[i][origen];
        (*mtrx).matriz[i][origen] = (*mtrx).matriz[i][destino];
        (*mtrx).matriz[i][destino] = swap;
    }
}

//corregir
/*
void intercambiar_filas_matriz(Matriz * mtrx, int origen, int destino){
    float swap;
    for (int i = 0; i < capacidad_columnas_matriz(mtrx);i++){
        swap = (*mtrx).matriz[origen][i];
        (*mtrx).matriz[origen][i] = (*mtrx).matriz[destino][i];
        (*mtrx).matriz[destino][i] = swap;
    }
}
*/

void intercambiar_filas_matriz(Matriz * mtrx, int origen, int destino){
    if ((*mtrx).filas >= origen && origen >= 0){
        if ((*mtrx).filas >= destino && destino >= 0){
            float * swap = (*mtrx).matriz[origen];
            (*mtrx).matriz[origen] = (*mtrx).matriz[destino];
            (*mtrx).matriz[destino] =swap;
        }
    }
}

void incertar_fila_matriz(Matriz * mtrx, int pos){
    if ((*mtrx).filas >= pos && pos >= 0){
        (*mtrx).matriz = realloc((*mtrx).matriz, sizeof(float*) * ((*mtrx).filas + 1));
        for(int i = (*mtrx).filas - 1; i >= pos; i--){
           (*mtrx).matriz[i+1] = (*mtrx).matriz[i];
        }
        (*mtrx).matriz[pos] = malloc(sizeof(float) * (*mtrx).columnas);
        (*mtrx).filas++;
    }
}

Matriz * sumar_matrices(Matriz * mtrx1, Matriz * mtrx2){
    Matriz * mtrx = NULL;
    if((*mtrx1).filas == (*mtrx2).filas){
        if((*mtrx1).columnas == (*mtrx2).columnas){
            mtrx = matriz_crear((*mtrx1).filas, (*mtrx1).columnas); 
            for(int i = 0; i < (*mtrx1).filas;i++){
                for (int j = 0; j < (*mtrx1).columnas; j++){
                    (*mtrx).matriz[i][j] = (*mtrx1).matriz[i][j] + (*mtrx2).matriz[i][j];
                }
            }
        }
    }
    return mtrx;
}

Matriz * multiplicar_matrices(Matriz * mtrx1, Matriz * mtrx2){
    Matriz * mtrx = NULL;
    if((*mtrx1).columnas == (*mtrx2).filas){
        mtrx = matriz_crear((*mtrx1).filas, (*mtrx2).columnas);
        float mult = 0;
        for(int i = 0; i < (*mtrx1).filas; i++){
            for (int j = 0; j < (*mtrx2).columnas; j++){
                for(int k = 0; k < (*mtrx1).columnas;k++){
                    mult += leer_matriz(mtrx1, i, k) * leer_matriz(mtrx1, k, j);
                }
                escribir_matriz(mtrx,i,j,mult);
                mult = 0;   
            }
        }
    }
    return mtrx;
}

int main(){

    Matriz * mtrx1 = matriz_crear(2,2);
    Matriz * mtrx2 = matriz_crear(2,2); 
    Matriz * mtrx;

    //incertar_fila_matriz(mtrx, 2);

    for (int i = 0; i < capacidad_filas_matriz(mtrx1); i++){
        for (int j = 0; j < capacidad_columnas_matriz(mtrx1);j++){
            escribir_matriz(mtrx1, i, j, i + j);
        }
    }

    for (int i = 0; i < capacidad_filas_matriz(mtrx2); i++){
        for (int j = 0; j < capacidad_columnas_matriz(mtrx2);j++){
            escribir_matriz(mtrx2, i, j, i + j);
        }
    }
    
    mtrx = multiplicar_matrices(mtrx1, mtrx2);

    matriz_imprimir(mtrx);
    printf("\n");
    matriz_destruir(mtrx);

    mtrx = sumar_matrices(mtrx1, mtrx2);

    matriz_imprimir(mtrx);
    printf("\n");
    matriz_destruir(mtrx);
    
    matriz_destruir(mtrx1);
    matriz_destruir(mtrx2);
    
    /*
    intercambiar_columnas_matriz(mtrx, 0, 2);
    matriz_imprimir(mtrx);
    printf("\n");

    intercambiar_filas_matriz(mtrx,0,4);
    matriz_imprimir(mtrx);
    printf("\n");
    

    matriz_destruir(mtrx);
    */
    

    return 1;
}
#include "bstree.h"
#include "cola.h"
#include <stdio.h>
#include <stdlib.h>

BSTree bstree_crear() {
  return NULL;
}

void bstree_destruir(BSTree nodo) {
  if (nodo != NULL) {
    bstree_destruir(nodo->left);
    bstree_destruir(nodo->right);
    free(nodo);
  }
}

int bstree_empty(BSTree nodo) {
  return nodo == NULL;
}

BSTree bstree_unir(int dato, BSTree left, BSTree right) {
  BSTree nuevoNodo = malloc(sizeof(BSTNodo));
  nuevoNodo->dato = dato;
  nuevoNodo->left = left;
  nuevoNodo->right = right;
  return nuevoNodo;
}

BSTree bstree_agregar(BSTree nodo, int dato){
	if (bstree_empty(nodo)) {
		nodo = malloc(sizeof(BSTNodo));
		nodo->left = NULL;
		nodo->right = NULL;
	}
	else if (nodo->dato > dato) 
		nodo->left = bstree_agregar(nodo->left, dato);
	else
		nodo->right = bstree_agregar(nodo->right, dato);
	return nodo;
}

BSTree recuprar_izq(BSTree nodo){
	if (nodo == NULL)
		return NULL;
	BSTree nod = recuprar_izq(nodo);
	return bstree_empty(nod) ? nodo : nod;
		
}

BSTree bstree_eliminar(BSTree nodo, int dato){
	if (bstree_empty(nodo))
		return NULL;
	if (nodo->dato == dato) {
		if (nodo->left != NULL && nodo->right != NULL)
			return recuprar_izq(nodo);
		if (nodo->left == NULL && nodo->right ==NULL){
			bstree_destruir(nodo);
			return NULL;
		}
		if (nodo->left != NULL)
			return nodo->left;
		else
			return nodo->right;
	}else if (nodo->dato > dato)
		bstree_eliminar(nodo->left, dato);
	else if (nodo->dato < dato)
		bstree_eliminar(nodo->right, dato);
	printf("%d\n",dato);
	return nodo;
}

int bstree_len(BSTree arbol){
	if (bstree_empty(arbol))
		return 0;
	return 1 + bstree_len(arbol->left) + bstree_len(arbol->right); 
	}

int bstree_altura(BSTree arbol, int i){
	if (bstree_empty(arbol))
		return i;
	int l = bstree_altura(arbol->left, i+1);
	int r = bstree_altura(arbol->right, i+1);
	return l < r ? r : l;
	}

void bstree_recorrer_bfs (BSTree arbol, 
						FuncionVisitanteExtra visit, void *extra){
	if (!bstree_empty(arbol)) {
		Cola cola = cola_crear();
		cola_encolar(cola, arbol);
		BSTree nodo;
		while (!cola_vacia(cola)) {
			nodo = cola_desencolar(cola);
			if (nodo->left != NULL)
				cola_encolar(cola, nodo->left);
			if (nodo->right != NULL)
				cola_encolar(cola, nodo->right);
			visit(nodo->dato, extra);
		}
		cola_destruir(cola);
	}
}

#include <stdio.h>
#include <stdlib.h>
#include "btree.h"

int btree_suma(BTree arbol){
	if (btree_empty(arbol))
		return 0;
	return arbol->dato + btree_suma(arbol->left) + btree_suma(arbol->right);
}

int btree_len(BTree arbol){
	if (btree_empty(arbol))
		return 0;
	return 1 + btree_len(arbol->left) + btree_len(arbol->right); 
	}

int btree_altura(BTree arbol, int i){
	if (btree_empty(arbol))
		return i;
	int l = btree_altura(arbol->left, i+1);
	int r = btree_altura(arbol->right, i+1);
	return l < r ? r : l;
	}

void mirror(BTree arbol){
	if (!btree_empty(arbol)){
		BTree aux;
		aux = arbol->left;
		arbol->left = arbol->right;
		arbol->right = aux;
		mirror(arbol->right);
		mirror(arbol->left);
		
		}
	}

static void imprimir_entero(int data) {
  printf("%d ", data);
}

static void sumar_contador(int dato, void * contador) {
  int * i = (int *) contador;
  *i = *i + dato;
}

static void mostrame_menos(int dato, void * resta) {
  printf("%d ", dato - *(int *)resta);
}

int main() {
  BTree ll = btree_unir(1, btree_crear(), btree_crear());
  BTree l = btree_unir(2, ll, btree_crear());
  BTree rrr = btree_unir(6, btree_crear(), btree_crear());
  BTree rr = btree_unir(5, btree_crear(), rrr);
  BTree r = btree_unir(3, btree_crear(), rr);
  BTree raiz = btree_unir(4, l, r);
  
	//Ejercicio 3
  btree_recorrer(raiz, BTREE_RECORRIDO_PRE, imprimir_entero);
  puts("");
  btree_recorrer(raiz, BTREE_RECORRIDO_POST, imprimir_entero);
  puts("");
  btree_recorrer(raiz, BTREE_RECORRIDO_IN, imprimir_entero);
  puts("");
  
  //Ejercicio 2
  printf("Suma Nodos: %d\n", btree_suma(raiz));
  printf("Cantidad de nodos: %d\n", btree_len(raiz));
  printf("Altura arbol: %d\n", btree_altura(raiz, 0));
  
  //Ejercii=cio 3
  int a = 0;
  btree_recorrer_extra(raiz, BTREE_RECORRIDO_PRE, sumar_contador, &a);
  printf("Suma nodos: %d\n",a);
  
  //Ejercicio 4
  a = 0;
  btree_recorrer_bfs(raiz, mostrame_menos, &a);
  puts("");
  mirror(raiz);
  btree_recorrer_bfs(raiz, mostrame_menos, &a);
  puts("");
  btree_recorrer_bfs(raiz, sumar_contador, &a);
  printf("Suma nodos: %d\n",a);
  
  btree_destruir(raiz);

  return 0;
}

// gcc -o practica1_3 -Wall -Werror practica1_3.c 
#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int* direccion;
    int capacidad;
} ArregloEnteros;

ArregloEnteros* arreglo_enteros_crear(int cap){
    ArregloEnteros * aeDinamico;
    ArregloEnteros ae;
    aeDinamico = malloc(sizeof(ArregloEnteros));
    ae.direccion = malloc(sizeof(int) * cap);
    ae.capacidad =  cap;
    *aeDinamico = ae;
    return aeDinamico;
}

void arreglo_enteros_destruir(ArregloEnteros* arreglo){
    ArregloEnteros ae = *arreglo;
    free(ae.direccion);
    free(arreglo);
    arreglo = NULL;
}

int arreglo_enteros_leer(ArregloEnteros* arreglo, int pos){
    int salida = 0, * intArreglo;
    ArregloEnteros ae = *arreglo;
    intArreglo = ae.direccion;
    if(ae.capacidad > pos && pos >= 0){
        salida = intArreglo[pos];
    }

    return salida;
}

void arreglo_enteros_imprimir(ArregloEnteros* arreglo){
    ArregloEnteros ae = *arreglo;
    int * intArreglo = ae.direccion, larg = ae.capacidad;
    printf("Long: %d\n", larg);
    if (intArreglo != NULL){
        for (int i = 0; i < larg;i++){
            printf("%d\n",intArreglo[i]);
        }
    }
}

void arreglo_enteros_escribir(ArregloEnteros* arreglo, int pos, int dato){
    ArregloEnteros ae = *arreglo;
    int * intArreglo = ae.direccion;
    if(ae.capacidad > pos && pos >= 0){
        intArreglo[pos] = dato;
    }

}

int arreglo_enteros_capacidad(ArregloEnteros* arreglo){
    ArregloEnteros ea = *arreglo;
    return ea.capacidad;
}

int main(){
    ArregloEnteros* ae = arreglo_enteros_crear(5);
    
    printf("%p\n",ae);
    for(int i = 0; i < arreglo_enteros_capacidad(ae);i++){arreglo_enteros_escribir(ae,i,2*i);}
    
    arreglo_enteros_imprimir(ae);

    arreglo_enteros_destruir(ae);

    


    return 1;
}